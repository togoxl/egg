'use strict';
const path = require('path');
/** @type Egg.EggPlugin */
module.exports = {
  // had enabled by egg
  // static: {
  //   enable: true,
  // }
  mongoose: {
    enable: true,
    package: 'egg-mongoose',
  },
  jwt: {
    enable: true,
    package: 'egg-jwt',
  },
  validate: {
    enable: true,
    package: 'egg-dora-validate',
  },
  //日志插件
  SystemOptionLog: {
    enable: true,
    package: 'egg-dora-systemoptionlog',
    path: path.join(__dirname, "../lib/plugin/egg-dora-systemoptionlog")
  },

  doraContent: {
    enable: true,
    package: 'egg-dora-content',
    path: path.join(__dirname, "../lib/plugin/egg-dora-content")
  },
  doraContentCategory : {
    enable: true,
    package: 'egg-dora-contentcategory',
    path: path.join(__dirname, "../lib/plugin/egg-dora-contentcategory")
},
doraContentTags : {
  enable: true,
  package: 'egg-dora-contenttags',
  path: path.join(__dirname, "../lib/plugin/egg-dora-contenttags")
},
doraContentMessage :{
  enable: true,
  package: 'egg-dora-contentmessage',
  path: path.join(__dirname, "../lib/plugin/egg-dora-contentmessage")
}

};

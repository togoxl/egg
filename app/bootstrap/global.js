const _ = require('lodash');
const shortid = require('shortid');
// const moment = require('moment');

// 校验合法ID
global.checkCurrentId = (ids) => {
  if (!ids) return false;
  let idState = true;
  let idsArr = ids.split(',');
  if (typeof idsArr === "object" && idsArr.length > 0) {
    for (let i = 0; i < idsArr.length; i++) {
      if (!shortid.isValid(idsArr[i])) {
        idState = false;
        break;
      }
    }
  } else {
    idState = false;
  }
  return idState;
}

require('module-alias/register')
//文件操作对象
let fs = require('fs');
let path = require('path');
let stat = fs.stat;
let iconv = require('iconv-lite');
var CryptoJS = require("crypto-js");
module.exports = {
    success(ctx, {
        data = {},
        message = ''
    } = {}) {
        ctx.body = {
            status: 200,
            data: data || {},
            message: message || ''
        }
        ctx.status = 200;
    },

    err(ctx, {
        message = '',
        data = {},
    } = {}) {

        if (message) {
            // throw new Error(message);
            if (message instanceof Object) {
                message = message.message;
            }
            ctx.body = {
                status: 500,
                message: message,
                data: data || {},
            }
            ctx.status = 200;
        } else {
            throw new Error(message);
        }

    },
    // 备用
    renderSuccess(ctx, {
        data = {},
        message = ''
    } = {}) {
        ctx.body = {
            status: 200,
            data: data || {},
            message: message || ''
        }
        ctx.status = 200;
    },

    renderFail(ctx, {
        message = '',
        data = {},
    } = {}) {

        if (message) {
            // throw new Error(message);
            if (message instanceof Object) {
                message = message.message;
            }
            ctx.body = {
                status: 500,
                message: message,
                data: data || {},
            }
            ctx.status = 200;
        } else {
            throw new Error(message);
        }

    },

    
    
    readFile(path) { // 文件读取
        return new Promise((resolve, reject) => {
            if (fs.existsSync(path)) {
                fs.readFile(path, "binary", function (error, data) {
                    if (error) {
                        console.log(error)
                        reject(error);
                    } else {
                        //处理中文乱码问题
                        let buf = new Buffer(data, 'binary');
                        let newData = iconv.decode(buf, 'utf-8');
                        resolve(newData);
                    }
                });
            } else {
                reject(this.ctx.__('validate_error_params'));
            }
        })
    },
    writeFile(path, content) {
        if (fs.existsSync(path)) {
            //写入文件
            let newContent = iconv.encode(content, 'utf-8');
            fs.writeFileSync(path, newContent);
            return 200;
        } else {
            return 500;
        }
    },
    decrypt(data, key) { //密码解密
        var bytes = CryptoJS.AES.decrypt(data, key);
        console.log(bytes.toString(CryptoJS.enc.Utf8))
        return bytes.toString(CryptoJS.enc.Utf8);
    },
    

 

};
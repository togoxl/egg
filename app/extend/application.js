'use strict';
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
// const muri = require('muri');
// const restore = require('@cdxoo/mongodb-restore')
// const child = require('child_process');

require('module-alias/register')
// const {
//     siteFunc
// } = require('@utils');
module.exports = {
    // 初始化数据模型
    initExtendModel(modelsPath) {
        let app = this;
        fs.readdirSync(modelsPath).forEach(function (extendName) {
            if (extendName) {
                let filePath = `${modelsPath}/${extendName}`;
                if (fs.existsSync(filePath)) {
                    let modelKey = path.basename(extendName.charAt(0).toUpperCase() + extendName.slice(1), '.js');
                    if (_.isEmpty(app.model[modelKey])) {
                        let targetModel = app.loader.loadFile(filePath);
                        app.model[modelKey] = targetModel;
                    }
                }
            }
        });
    },
    // 初始化插件路由
    async initPluginRouter(ctx, pluginConfig = {}, pluginManageController = {}, pluginApiController = {}, next = {}) {

        let app = this;
        let isFontApi = false;
        let isAdminApi = false;
        let targetControllerName = '';
        let targetApiItem = {};
        if (!_.isEmpty(pluginConfig)) {

            if (!_.isEmpty(pluginConfig)) {
                let {
                    adminApi,
                    fontApi
                } = pluginConfig;
                let targetRequestUrl = ctx.request.url;
                if (targetRequestUrl.indexOf('/api/') >= 0) {

                    for (const fontApiItem of fontApi) {
                        let {
                            url,
                            method,
                            controllerName
                        } = fontApiItem;

                        let targetApi = targetRequestUrl.replace('/api/', '').split("?")[0];
                        if (ctx.request.method == method.toUpperCase() && targetApi === url && controllerName) {
                            isFontApi = true;
                            targetControllerName = controllerName;
                            targetApiItem = fontApiItem;
                            break;
                        }

                    }

                } else if (targetRequestUrl.indexOf('/manage/') >= 0) {

                    for (const adminApiItem of adminApi) {

                        let {
                            url,
                            method,
                            controllerName
                        } = adminApiItem;

                        let targetApi = targetRequestUrl.replace('/manage/', '').split("?")[0];
                        if (ctx.request.method == method.toUpperCase() && targetApi === url && controllerName) {
                            isAdminApi = true;
                            targetControllerName = controllerName;
                            targetApiItem = adminApiItem;
                            break;
                        }

                    }
                }

            }

        }

        if (isAdminApi && !_.isEmpty(pluginManageController) && targetControllerName) {
            await pluginManageController[targetControllerName](ctx, app);
        } else if (isFontApi && !_.isEmpty(pluginApiController) && targetControllerName) {
            if (targetApiItem.authToken) {
                if (ctx.session.logined) {
                    await pluginApiController[targetControllerName](ctx, app, next);
                } else {
                    ctx.helper.renderFail(ctx, {
                        message: ctx.__('label_notice_asklogin')
                    });
                }
            } else {
                await pluginApiController[targetControllerName](ctx, app, next);
            }
        }

    },
};
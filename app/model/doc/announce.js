module.exports = app => {
    const mongoose = app.mongoose
    var shortid = require('shortid');
    var Schema = mongoose.Schema;
    var moment = require('moment')

    var AnnounceSchema = new Schema({
        _id: {
            type: String,
            'default': shortid.generate
        },
        date:{
            type:Date,
            default: Date.now 
        },
        //标题
        noticeTitle:{
            type:String,
        },
        //创建人
        createBy:{
            type:String
        },
        //创建时间
        createTime:{
            type: Date,
            default: Date.now 
        },
        //内容
        noticeContent:{
            type:String
        },
        // 状态
        status:{
            type:String
        },
        //类别
        noticeType:{
            type:String
        },
        //更新时间
        updateTime:{
            type: Date,
        },
        //更新人
        updateBy:{
            type:String
        },
    });


    AnnounceSchema.set('toJSON', {
        getters: true,
        virtuals: true
    });
    AnnounceSchema.set('toObject', {
        getters: true,
        virtuals: true
    });

    AnnounceSchema.path('createTime').get(function (v) {
        return moment(v).format("YYYY-MM-DD HH:mm:ss");
    });
    AnnounceSchema.path('updateTime').get(function (v) {
        return moment(v).format("YYYY-MM-DD HH:mm:ss");
    });

    return mongoose.model("Announce", AnnounceSchema, 'announces');

}
module.exports = app => {
    var moment = require('moment')
    const mongoose = app.mongoose
    var Schema = mongoose.Schema
    
    var DictDetailsSehema = new Schema({

        // createBy: "" 创建人
        // createTime: "" //创建时间
        // dictCode: 2    //编码
        // dictLabel: "女" //字典name
        // dictSort: 2     //排序
        // dictType: "sys_user_sex" //唯一值
        // dictValue: "1"  //字典value
        // remark: "性别女" //备注
        // searchValue: null
        // status: "0"     //状态
        // updateBy: null  //更新人
        // updateTime: null //更新时间
        createBy:String,
        createTime:{
            type:Date,
            define:Date.now
        },
        dictLabel:String,
         dictSort: Number,
        dictType: {
            type:String,
            requier:true
        },
        dictValue:  String,
        remark: String,
        // searchValue: null
        status: String,
        updateBy: String,
        updateTime: Date
    })
    DictDetailsSehema.set('toJSON', {
        getters: true,
        virtuals: true
    });
    DictDetailsSehema.set('toObject', {
        getters: true,
        virtuals: true
    });

    DictDetailsSehema.path('createTime').get(function (v) {
        return moment(v).format("YYYY-MM-DD HH:mm:ss");
    });
    DictDetailsSehema.path('updateTime').get(function (v) {
        return moment(v).format("YYYY-MM-DD HH:mm:ss");
    });
    return mongoose.model("DictDetails",DictDetailsSehema,'dictDetails')
}
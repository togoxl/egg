module.exports=app=>{
    var shortid = require('shortid');
    var moment = require('moment')
    const mongoose=app.mongoose
    var Schema=mongoose.Schema
    
    // createBy  创建人
    // createTime   创建时间
    // dictId        id
    // dictName      字典名称
    // dictType      字典类型
    // remark        备注
    // status        停用--启用
    // updateBy      更新人
    // updateTime    更新时间
    var dictTyppeSchema=new Schema({
        createBy:String,
        createTime:{
            type:Date,
            default: Date.now
        },
        dictId:{
            type:String,
            'default': shortid.generate
        },

        dictName:String,
        dictType:{
            type:String,
            required:true
        },
        remark:String,
        status:String,
        updateBy:String,
        updateTime:{
            type:Date
        }

    })
    dictTyppeSchema.set('toJSON', {
        getters: true,
        virtuals: true
    });
    dictTyppeSchema.set('toObject', {
        getters: true,
        virtuals: true
    });

    dictTyppeSchema.path('createTime').get(function (v) {
        return moment(v).format("YYYY-MM-DD HH:mm:ss");
    });
    dictTyppeSchema.path('updateTime').get(function (v) {
        return moment(v).format("YYYY-MM-DD HH:mm:ss");
    });

    return mongoose.model("DictType", dictTyppeSchema, 'dictType');
}
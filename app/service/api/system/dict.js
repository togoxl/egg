'use strict';

const Service = require('egg').Service;
const path = require('path')
const _ = require('lodash')

// general是一个公共库，可用可不用
const {
    _list,
    _item,
    _count,
    _create,
    _update,
    _removes,
    _safeDelete
} = require(path.join(process.cwd(), 'app/service/general'));
class DictService extends Service {
    async find(payload, {
        query = {},
        searchKeys = [],
        populate = [],
        files = null
    } = {}) {
        let listdata = _list(this.ctx.model.Dict.Type, payload, {
            sort : {
                createTime: -1
            },
            query: query,
            searchKeys: searchKeys,
            populate: populate,
            files
        });
        return listdata;

    }


    async count(params = {}) {
        return _count(this.ctx.model.Dict.Type, params);
    }

    async create(payload) {
        // console.log(this.ctx.model)
        return _create(this.ctx.model.Dict.Type, payload);
    }

    async removes(res, values, key = '_id') {
        return _removes(res, this.ctx.model.Dict.Type, values, key);
    }

    async safeDelete(res, values) {
        return _safeDelete(res, this.ctx.model.Announce, values);
    }

    async update(res, _id, payload) {
        return _update(res, this.ctx.model.Dict.Type, _id, payload);
    }

    async item(res, params = {}) {
        return _item(res, this.ctx.model.Dict.Type, params)
    }

    // 字典详情
    async findDetails(payload, {
        query = {},
        searchKeys = [],
        populate = [],
        files = null
    } = {}) {
        let listdata = _list(this.ctx.model.Dict.Details, payload, {
            query: query,
            searchKeys: searchKeys,
            populate: populate,
            files
        });
        return listdata;

    }


    async countDetails(params = {}) {
        return _count(this.ctx.model.Dict.Details, params);
    }

    async createDetails(payload) {
        // console.log(this.ctx.model)
        return _create(this.ctx.model.Dict.Details, payload);
    }

    async removesDetails(res, values, key = '_id') {
        return _removes(res, this.ctx.model.Dict.Details, values, key);
    }

    async updateDetails(res, _id, payload) {
        return _update(res, this.ctx.model.Dict.Details, _id, payload);
    }

    async itemDetails(res, params = {}) {
        return _item(res, this.ctx.model.Dict.Details, params)
    }
}

module.exports = DictService;

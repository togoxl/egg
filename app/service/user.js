'use strict';

const Service = require('egg').Service;
const svgCaptcha = require('svg-captcha'); //验证码

class UserService extends Service {
    async captcha() {
        let m = this;
        // 第三方插件，
        const captcha = svgCaptcha.create({
            size: 1,                 	// 长度(显示几个字符)
            fontSize: 50,           	// 字体大小
            width: 100,              	// 宽度
            height: 58,            		// 高度
            background: '#c9cdcd',   	// 背景颜色
        });
        // 将验证码text文本保存到全局session中
        m.ctx.session.code = captcha.text.toLowerCase();
        //验证码存redis
        // await m.app.redis.get('db1').set(m.ctx.request.ip,captcha.text,"EX",60)  //ex是写死的，60是秒
        return captcha;
    }
    async getUser(params){
        const {ctx}=this
        return await ctx.model.User.findOne({
            userName: params.userName,
            passWord: params.passWord
        })
    }
}

module.exports = UserService;

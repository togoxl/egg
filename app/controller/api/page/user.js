'use strict';

const Controller = require('egg').Controller;
const {
    adminGroupRule
} = require('@validate')

const _ = require('lodash');

class UserController extends Controller {
    async getList() {
        const {
            ctx,
            service
        } = this;
        try {
            let payload = ctx.query;
            let adminGroupList = await ctx.service.api.adminGroup.find(payload);

            ctx.helper.success(ctx, {
                data: adminGroupList
            });

        } catch (err) {

            ctx.helper.err(ctx, {
                message: err
            });

        }
    };
    async create() {

        const {
            ctx,
            service
        } = this;
        try {


            let fields = ctx.request.body || {};

            const formObj = {
                name: fields.name,
                comments: fields.comments
            }


            ctx.validate(adminGroupRule.form(ctx), formObj);

            

            await ctx.service.api.adminGroup.create(formObj);

            ctx.helper.success(ctx);

        } catch (err) {
            ctx.helper.err(ctx, {
                message: err
            });
        }

    }
    async getOne() {
        const {
            ctx,
            service
        } = this;
        try {
            let _id = ctx.query.id;
            let targetUser = await ctx.service.api.adminGroup.item(ctx, {
                query: {
                    _id: _id
                }
            });

            ctx.helper.success(ctx, {
                data: targetUser
            });

        } catch (err) {
            ctx.helper.err(ctx, {
                message: err
            });
        }

    }
    async update() {

        const {
            ctx,
            service
        } = this;
        try {

            let fields = ctx.request.body || {};
            const formObj = {
                name: fields.name,
                comments: fields.comments,
                // power: fields.power
            }


            ctx.validate(adminGroupRule.form(ctx), formObj);

            

            await ctx.service.api.adminGroup.update(ctx, fields._id, formObj);

            ctx.helper.success(ctx,{
                message:"更新成功"
            });

        } catch (err) {

            ctx.helper.err(ctx, {
                message: err
            });

        }


    }
    async removes() {
        const {
            ctx,
            service
        } = this;
        try {
            let targetIds = ctx.query.ids;
            await ctx.service.api.adminGroup.removes(ctx, targetIds);
            ctx.helper.success(ctx);

        } catch (err) {

            ctx.helper.err(ctx, {
                message: err
            });
        }
    }

}

module.exports = UserController;

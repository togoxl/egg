'use strict';

const Controller = require('egg').Controller;
const {
    adminUserRule
} = require('@validate')
const {
    siteFunc
} = require('@utils');
const fs = require('fs')

const {
    validatorUtil
} = require('@utils');

const path = require('path');
const _ = require('lodash');


class AdminUserController extends Controller {
    async list() {
        const {
            ctx,
            service
        } = this;
        try {
            let payload = ctx.query;
            let adminUserList = await ctx.service.api.adminUser.find(payload, {
                query: {
                    state: '1'
                },
                populate: [{
                    path: 'group',
                    select: "name _id"
                }],
                files: {
                    password: 0
                }
            });

            ctx.helper.renderSuccess(ctx, {
                data: adminUserList
            });

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });

        }
    }
    async getOne() {
        const {
            ctx,
            service
        } = this;
        try {
            let _id = ctx.query.id;
            let password = ctx.query.password;
            let queryObj = {
                _id
            };

            if (password) {
                _.assign(queryObj, {
                    password
                });
            }
            let targetItem = await ctx.service.api.adminUser.item(ctx, {
                query: queryObj,
                files: '-password'
            });

            ctx.helper.renderSuccess(ctx, {
                data: targetItem
            });

        } catch (err) {
            ctx.helper.renderFail(ctx, {
                message: err
            });
        }

    }
    async create() {

        const {
            ctx,
            service
        } = this;
        try {

            let fields = ctx.request.body || {};
            const formObj = {
                userName: fields.userName,
                name: fields.name,
                email: fields.email,
                phoneNum: fields.phoneNum,
                countryCode: fields.countryCode,
                password: fields.password,
                confirm: fields.confirm,
                group: fields.group,
                enable: fields.enable,
                comments: fields.comments
            }


            ctx.validate(adminUserRule.form(ctx), formObj);
            let oldItem = await ctx.service.api.adminUser.item(ctx, {
                query: {
                    userName: fields.userName
                }
            })

            if (!_.isEmpty(oldItem)) {
                throw new Error(ctx.__('validate_hadUse_userName'));
            }

            await ctx.service.api.adminUser.create(formObj);

            ctx.helper.renderSuccess(ctx);
        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });
        }
    }
    async update() {

        const {
            ctx,
            service
        } = this;
        try {

            let fields = ctx.request.body || {};
            const formObj = {
                userName: fields.userName,
                name: fields.name,
                email: fields.email,
                logo: fields.logo,
                phoneNum: fields.phoneNum,
                countryCode: fields.countryCode,
                group: fields.group,
                enable: fields.enable,
                comments: fields.comments
            }


            ctx.validate(adminUserRule.form(ctx), formObj);;

            // 单独判断密码
            if (fields.password) {
                if (!validatorUtil.checkPwd(fields.password)) {
                    errInfo = ctx.__("validate_inputCorrect", [ctx.__("label_password")])
                } else {
                    formObj.password = fields.password;
                }
            }

            let oldResource = await ctx.service.api.adminUser.item(ctx, {
                query: {
                    userName: fields.userName
                }
            })

            if (!_.isEmpty(oldResource) && oldResource._id != fields._id) {
                throw new Error(ctx.__("validate_hadUse_userName"));
            }
            await ctx.service.api.adminUser.update(ctx, fields._id, formObj);

            ctx.helper.renderSuccess(ctx);

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });

        }

    }


    async removes() {
        const {
            ctx,
            service
        } = this;
        try {
            let targetId = ctx.query.ids;
            // TODO 目前只针对删除单一管理员逻辑
           
            let oldUser = await ctx.service.api.adminUser.item(ctx, {
                query: {
                    _id: targetId
                }
            });
            
            let leftAdminUser = await ctx.service.api.adminUser.count();
            // console.log(leftAdminUser)
            // if (!_.isEmpty(oldUser)) {
            //     console.log(ctx.session.adminUserInfo._id)
            //     if (oldUser._id === ctx.session.adminUserInfo._id ||
            //         leftAdminUser == 1) {
            //         throw new Error("当前场景不允许删除该管理员用户");
            //     }
            // } else {
            //     throw new Error(ctx.__('validate_error_params'));
            // }
            await ctx.service.api.adminUser.removes(ctx, targetId);
            ctx.helper.renderSuccess(ctx);

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });
        }
    }

}

module.exports = AdminUserController;

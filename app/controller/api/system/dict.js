'use strict';

const Controller = require('egg').Controller;

class DictController extends Controller {
    async list() {
        const { ctx } = this
        try {
            let payload = ctx.query;
            let notifyList = await ctx.service.api.system.dict.find(payload, {
                searchKeys:['dictName'],
            });

            ctx.helper.renderSuccess(ctx, {
                data: notifyList
            });

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });

        }
    }
    async create() {
        const { ctx } = this
        try {
            let fields = ctx.request.body || {};
            let getOne = await ctx.service.api.system.dict.find({}, {
                query: {
                    dictType: fields.dictType
                }
            })
            if (getOne.list.length == 0) {
                // createBy  创建人
                // createTime   创建时间
                // dictId        id
                // dictName      字典名称
                // dictType      字典类型
                // remark        备注
                // status        停用--启用
                // updateBy      更新人
                // updateTime    更新时间
                const formObj = {
                    createBy: '默认',
                    dictName: fields.dictName,
                    dictType: fields.dictType,
                    remark: fields.remark,
                    status: fields.status
                }
                await ctx.service.api.system.dict.create(formObj);

                ctx.helper.renderSuccess(ctx);

            } else {
                ctx.helper.renderFail(ctx, {
                    message: "当前类型已存在"
                });
            }



        } catch (err) {
            ctx.helper.renderFail(ctx, {
                message: err
            });
        }

    }
    async getOne() {
        const { ctx } = this
        try {
            let _id = ctx.query.id;

            let targetUser = await ctx.service.api.system.dict.item(ctx, {
                query: {
                    _id: _id
                }
            });

            ctx.helper.renderSuccess(ctx, {
                data: targetUser
            });

        } catch (err) {
            ctx.helper.renderFail(ctx, {
                message: err
            });
        }

    }
    async removes() {
        const { ctx } = this
        try {

            let targetIds = ctx.query.ids;
            await ctx.service.api.system.dict.removes(ctx, targetIds);
            ctx.helper.renderSuccess(ctx);

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });
        }
    }
    async update() {

        const {
            ctx,
            service
        } = this;
        try {

            let fields = ctx.request.body || {};
            const formObj = {
                dictName: fields.dictName,
                dictType: fields.dictType,
                remark: fields.remark,
                status: fields.status,
                updateTime: new Date(),
                updateBy: 'lisi'

            }
            await ctx.service.api.system.dict.update(ctx, fields._id, formObj);

            ctx.helper.success(ctx, {
                message: "更新成功"
            });

        } catch (err) {

            ctx.helper.err(ctx, {
                message: err
            });

        }


    }
    // 字典详情
    async listDetails() {
        const { ctx } = this
        try {
            let payload = ctx.query;
            let notifyList = await ctx.service.api.system.dict.findDetails(payload, {
                query:{
                    dictType:payload.dictType
                }
            });

            ctx.helper.renderSuccess(ctx, {
                data: notifyList
            });

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });

        }
    }
    async createDetails() {
        const { ctx } = this
        try {
            let fields = ctx.request.body || {};
            // createBy: "" 创建人
        // dictCode: 2    //编码
        // dictLabel: "女" //字典name
        // dictSort: 2     //排序
        // dictType: "sys_user_sex" //唯一值
        // dictValue: "1"  //字典value
        // remark: "性别女" //备注
        // searchValue: null
        // status: "0"     //状态
        // updateBy: null  //更新人
        // updateTime: null //更新时间
                const formObj = {
                    createBy: '默认1',
                    dictSort: fields.dictSort,
                    dictType:fields.dictType,
                    dictLabel: fields.dictLabel,
                    dictValue: fields.dictValue,
                    remark: fields.remark,
                    status: fields.status,
                    createTime:new Date()
                }
                await ctx.service.api.system.dict.createDetails(formObj);

                ctx.helper.renderSuccess(ctx);
        } catch (err) {
            ctx.helper.renderFail(ctx, {
                message: err
            });
        }

    }
    async getOneDetails() {
        const { ctx } = this
        try {
            let _id = ctx.query.id;

            let targetUser = await ctx.service.api.system.dict.itemDetails(ctx, {
                query: {
                    _id: _id
                }
            });

            ctx.helper.renderSuccess(ctx, {
                data: targetUser
            });

        } catch (err) {
            ctx.helper.renderFail(ctx, {
                message: err
            });
        }

    }
    async removesDetails() {
        const { ctx } = this
        try {

            let targetIds = ctx.query.ids;
            await ctx.service.api.system.dict.removesDetails(ctx, targetIds);
            ctx.helper.renderSuccess(ctx);

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });
        }
    }
    async updateDetails() {

        const {
            ctx,
            service
        } = this;
        try {

            let fields = ctx.request.body || {};
            const formObj = {
                createBy: '默认123',
                dictSort: fields.dictSort,
                dictLabel: fields.dictLabel,
                dictValue: fields.dictValue,
                remark: fields.remark,
                status: fields.status,
                updateTime: new Date(),
                updateBy: '王五'

            }
            await ctx.service.api.system.dict.updateDetails(ctx, fields._id, formObj);

            ctx.helper.success(ctx, {
                message: "更新成功"
            });

        } catch (err) {

            ctx.helper.err(ctx, {
                message: err
            });

        }


    }
}

module.exports = DictController;

'use strict';
const Controller = require('egg').Controller;
const _ = require('lodash');
var CryptoJS = require("crypto-js");
const {
    adminUserRule
} = require('@validate')

class LoginController extends Controller {
    //获取验证码
    async getCode() {
        const { ctx } = this;
        // service方法
        let captcha = await this.service.user.captcha();
        // 返回的类型格式
        ctx.response.type = 'image/svg+xml';
        ctx.helper.success(ctx, {
            data: {
                code: captcha.data
            }
        })
    }
    //登陆
    async login() {
        const {
            ctx,
            service,
            app
        } = this;
        try {
            let fields = ctx.request.body || {};
            const systemConfigs = await ctx.service.systemConfig.find({
                isPaging: '0'
            });

            const {
                showImgCode
            } = systemConfigs[0];
           
            let errMsg = '';
            if (showImgCode && (!fields.code || fields.code != ctx.session.code)) {
                errMsg = ctx.__("验证码错误", [ctx.__("label_user_imageCode")])
            }
            
            if (errMsg) {
                throw new Error(errMsg);
            }
            const formObj = {
                userName: fields.userName
            }
            ctx.validate(adminUserRule.login(ctx), Object.assign({}, formObj, {
                password: fields.password
            }))
            let user = await ctx.service.api.adminUser.item(ctx, {
                query: formObj,
                populate: [{
                    path: 'group',
                    select: 'power _id enable name'
                }, {
                    path: 'targetEditor',
                    select: 'userName _id'
                }],
                files: 'enable password _id email userName '
            })
           
            if (!_.isEmpty(user)) {

                let userPsd = user.password;
                // 兼容老的加密方式
                // if (userPsd !== CryptoJS.MD5(this.app.config.salt_md5_key + fields.password).toString() &&
                //     fields.password != ctx.helper.decrypt(userPsd, this.app.config.encrypt_key)) {
                //     throw new Error(ctx.__("validate_login_notSuccess"));
                // }
                if(userPsd!==fields.password){
                    throw new Error(ctx.__("账号密码错误"));
                }

                if (!user.enable) {
                    throw new Error(ctx.__("validate_user_forbiden"));
                }

                const token=app.jwt.sign({
                    userId: user._id,
                }, app.config.jwt.secret, { expiresIn: '60m' });

                

                // // 记录登录日志
                let clientIp = ctx.header['x-forwarded-for'] || ctx.header['x-real-ip'] || ctx.request.ip;
                let loginLog = {
                    type: 'login',
                    logs: user.userName + ' login，ip:' + clientIp,
                };

                if (!_.isEmpty(ctx.service.systemOptionLog)) {
                    await ctx.service.systemOptionLog.create(loginLog);
                }

                ctx.helper.renderSuccess(ctx, {
                    data: {
                        token
                    }
                });

            } else {
                ctx.helper.renderFail(ctx, {
                    message: ctx.__("validate_login_notSuccess")
                });
            }



        } catch (err) {
            ctx.helper.renderFail(ctx, {
                message: err
            });
        }
        // const { ctx, app } = this
        // const { userName, passWord, code } = ctx.request.body;
        // let codes = ctx.session.code

        // if (codes == code) {
        //     const user = await ctx.service.user.getUser({ userName, passWord });
        //     if (user) {
        //         // 用户存在,生成token
        //         const token = app.jwt.sign({
        //             userId: user._id,
        //         }, app.config.jwt.secret, { expiresIn: '60m' });
        //          // 记录登录日志
        //          let clientIp = ctx.header['x-forwarded-for'] || ctx.header['x-real-ip'] || ctx.request.ip;
        //          let loginLog = {
        //              type: 'login',
        //              logs: user.userName + ' login，ip:' + clientIp,
        //          };
        //          if (!_.isEmpty(ctx.service.systemOptionLog)) {
        //             await ctx.service.systemOptionLog.create(loginLog);
        //         }
        //         ctx.helper.success(ctx,{
        //             data:{
        //                 token: token,  
        //             },
        //             message:"登录成功"
        //         })
        //     } else {
        //         ctx.helper.err(ctx,{
        //             message: '用户不存在/密码错误',
        //         })
        //     }
        // } else {
        //     ctx.helper.err(ctx,{
        //         message: '验证码不正确',
        //     })
        // }

    }
    //获取用户信息
    async getUserInfo() {
        const { ctx, app } = this
        ctx.helper.success(ctx, {
            data: {
                user: {
                    roles: ['admin'],
                    introduction: 'I am a super administrator',
                    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
                    name: 'Super Admin'
                }
            }
        })
    }
}

module.exports = LoginController;

'use strict';

const Controller = require('egg').Controller;

class AnnounceController extends Controller {
    async list() {
        const{ctx} =this
        try {
            let payload = ctx.query;
            let notifyList = await ctx.service.api.doc.announce.find(payload,{
                searchKeys:['noticeTitle']
            });

            ctx.helper.renderSuccess(ctx, {
                data: notifyList
            });

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });

        }
    }

    async create() {
       
        const{ctx} =this
        try {
            
            let fields = ctx.request.body || {};
            const formObj = {
                noticeTitle: fields.noticeTitle,
                noticeContent: fields.noticeContent,
                createBy: 'admin',
                status:fields.status,
                noticeType:fields.noticeType
            }
             await ctx.service.api.doc.announce.create(formObj);

            ctx.helper.renderSuccess(ctx);

        } catch (err) {
            ctx.helper.renderFail(ctx, {
                message: err
            });
        }

    }



    async getOne() {
        const{ctx} =this
        try {
            let _id = ctx.query.id;

            let targetUser = await ctx.service.api.doc.announce.item(ctx, {
                query: {
                    _id: _id
                }
            });

            ctx.helper.renderSuccess(ctx, {
                data: targetUser
            });

        } catch (err) {
            ctx.helper.renderFail(ctx, {
                message: err
            });
        }

    }


    async removes() {
        const{ctx} =this
        try {

            let targetIds = ctx.query.ids;
            await ctx.service.api.doc.announce.removes(ctx, targetIds);
            ctx.helper.renderSuccess(ctx);

        } catch (err) {

            ctx.helper.renderFail(ctx, {
                message: err
            });
        }
    }
    async update() {

        const {
            ctx,
            service
        } = this;
        try {

            let fields = ctx.request.body || {};
            const formObj = {
                noticeTitle: fields.noticeTitle,
                noticeContent: fields.noticeContent,
                createBy: 'admin',
                status:fields.status,
                noticeType:fields.noticeType,
                updateTime:new Date(),
                updateBy:'张三'
                
            }

            

            await ctx.service.api.doc.announce.update(ctx, fields._id, formObj);

            ctx.helper.success(ctx,{
                message:"更新成功"
            });

        } catch (err) {

            ctx.helper.err(ctx, {
                message: err
            });

        }


    }
}

module.exports = AnnounceController;

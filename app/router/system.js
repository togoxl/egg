module.exports=app=>{
    const {router,controller}=app
    //获取验证码
  router.get('/api/getCode', controller.api.login.getCode);
  //登陆
  router.post('/api/login', controller.api.login.login);
  //获取用户信息
  router.get('/api/getUserInfo', controller.api.login.getUserInfo);
  //用户增删改查
  // router.get('/api/getUserList', controller.api.page.user.getList);
  // router.post('/api/createUser', controller.api.page.user.create);
  // router.get('/api/getUserOne', controller.api.page.user.getOne);
  // router.post('/api/updateUser', controller.api.page.user.update);
  // router.get('/api/deleteUser', controller.api.page.user.removes);
  //系统用户增删改查
  router.get('/api/system/getUserList', controller.api.system.adminUser.list);
  router.get('/api/system/getUserOne', controller.api.system.adminUser.getOne);
  router.post('/api/system/createUser', controller.api.system.adminUser.create);
  router.post('/api/system/updateUser', controller.api.system.adminUser.update);
  router.get('/api/system/delete', controller.api.system.adminUser.removes);
  /**
     * 资源管理
     * 
     */

    router.get('/api/adminResource/getList', controller.api.system.adminResource.list)

    router.get('/api/adminResource/getOne', controller.api.system.adminResource.getOne)

    router.post('/api/adminResource/addOne', controller.api.system.adminResource.create)

    router.post('/api/adminResource/updateOne', controller.api.system.adminResource.update)

    router.post('/api/adminResource/updateParentId', controller.api.system.adminResource.updateParentId)

    router.get('/api/adminResource/deleteResource', controller.api.system.adminResource.removes)

    router.get('/api/adminResource/getListByPower', controller.api.system.adminResource.listByPower)
    /**
     * 角色管理
     */
    router.get('/api/adminGroup/getList', controller.api.system.adminGroup.list)

    router.get('/api/adminGroup/getOne', controller.api.system.adminGroup.getOne)

    router.post('/api/adminGroup/addOne', controller.api.system.adminGroup.create)

    router.post('/api/adminGroup/updateOne', controller.api.system.adminGroup.update)

    router.get('/api/adminGroup/deleteGroup', controller.api.system.adminGroup.removes)
    /**
     * 字典管理
     */
    router.post('/api/dict/addOne', controller.api.system.dict.create)

    router.get('/api/dict/getList', controller.api.system.dict.list)

    router.get('/api/dict/getOne', controller.api.system.dict.getOne)

    router.post('/api/dict/update', controller.api.system.dict.update)

    router.get('/api/dict/delete', controller.api.system.dict.removes)
    // 详情
    router.post('/api/dictDetails/addOne', controller.api.system.dict.createDetails)

    router.get('/api/dictDetails/getList', controller.api.system.dict.listDetails)

    router.get('/api/dictDetails/getOne', controller.api.system.dict.getOneDetails)

    router.post('/api/dictDetails/update', controller.api.system.dict.updateDetails)

    router.get('/api/dictDetails/delete', controller.api.system.dict.removesDetails)
    
}
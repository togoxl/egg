module.exports=app=>{
    const {router,controller}=app
        /**
     * 系统公告
     */
    router.post('/api/announce/addOne', controller.api.doc.announce.create)
    router.get('/api/announce/getList', controller.api.doc.announce.list)

    router.get('/api/announce/getOne', controller.api.doc.announce.getOne)

    

    router.post('/api/announce/update', controller.api.doc.announce.update)

    router.get('/api/announce/delete', controller.api.doc.announce.removes)
}
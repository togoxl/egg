'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller,jwt } = app;
    // 系统
    require('./router/system')(app)
    //文档
    require("./router/doc")(app)
};

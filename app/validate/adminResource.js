
const form = (ctx) => {

    return {

        type: {
            type: "string",
            required: true,
            message: ctx.__("validate_inputCorrect", [ctx.__("label_resourceType")])
        },
        comments: {
            type: "string",
            required: true,
            min: 2,
            max: 30,
            message: ctx.__("validate_inputCorrect", [ctx.__("label_comments")])
        },
    }

}

module.exports = {
    form
}
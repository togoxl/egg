
const form = (ctx) => {
    return {
        name: {
            type: "string",
            required: true,
            min: 2,
            max: 50,
            message: ctx.__("validate_error_field", [ctx.__("label_name")])
        },
        comments: {
            type: "string",
            required: true,
            message: ctx.__("validate_inputCorrect", [ctx.__("label_comments")])
        }
    }
}


module.exports = {
    form
}